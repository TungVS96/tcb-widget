const path = require('path');
const fs = require('fs');
const JavaScriptObfuscator = require('webpack-obfuscator');
const appDirectory = fs.realpathSync(process.cwd());
const resolveApp = relativePath => path.resolve(appDirectory, relativePath);

module.exports = {
    devtool: 'inline-source-map',
    entry: {
        'pre-approved': './src/index.tsx'
    },
    output: {
        filename: "[name]-widget.js",
        path: path.resolve(__dirname, 'public'),
    },
    mode: 'production',
    module: {
        rules: [{
            oneOf: [{
                    test: /\.(png|svg|jpg|jpeg|gif)$/i,
                    loader: require.resolve('url-loader'),
                    options: {
                        // limit: 10000,
                        name: 'static/media/[name].[hash:8].[ext]'
                    }
                },
                {
                    test: /\.(js|ts|tsx)$/,
                    include: resolveApp('src'),
                    use: [{
                            loader: require.resolve('babel-loader'),
                            options: {
                                compact: true,
                                cacheDirectory: true
                            }
                        },
                        {
                            loader: require.resolve('ts-loader'),
                            options: {
                                transpileOnly: true,
                                experimentalWatchApi: true
                            }
                        }
                    ]
                },
                {
                    test: /\.css$/,
                    use: [
                        require.resolve('style-loader'),
                        {
                            loader: require.resolve('css-loader'),
                            options: {
                                importLoaders: 1
                            }
                        },
                        {
                            loader: require.resolve('postcss-loader'),
                            options: {
                                ident: 'postcss',
                                plugins: () => [
                                    require('postcss-flexbugs-fixes')
                                ]
                            }
                        }
                    ]
                },
                {
                    exclude: [/\.(js|jsx|mjs)$/, /\.html$/, /\.json$/],
                    loader: require.resolve('file-loader'),
                    options: {
                        name: 'static/media/[name].[hash:8].[ext]'
                    }
                }
            ]
        }],
    },
    resolve: {
        extensions: ['.tsx', '.ts', '.js'],
    },
    plugins: [
        // new JavaScriptObfuscator(),
    ]
};