import logo from './assets/ic-tcb-logo.png'
import logo2x from './assets/ic-tcb-logo@2x.png'
import logo3x from './assets/ic-tcb-logo@3x.png'

const logoTechcombank = () =>
    <img
        src={logo}
        srcSet={`${logo}, ${logo2x} 2x, ${logo3x} 3x`}
        width={195}
        height={28}
        style={{ marginTop: '8px', marginLeft: '16px' }}
    />

export { logoTechcombank }