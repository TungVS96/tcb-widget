import { STEP } from './PreApproved.type';

export default {
	GO_NEXT: (props: any) => (step: STEP) => {
		console.log('GO_NEXT', { props });
		props.setStep(step);
	}
};
