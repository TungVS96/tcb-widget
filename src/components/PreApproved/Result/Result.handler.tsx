
export default {
	ON_GET_PRE_APPROVED_RESULT_SUCCESS: (props: any) => (response: any) => {
		props.setPreApprovedResult(response.approvalResult);
	}
};
