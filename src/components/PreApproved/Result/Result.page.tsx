import Result from './Result.view'
import { compose, withState, withHandlers } from 'recompose'
import handlers  from './Result.handler'

export default compose(
    withState('preApprovedResult', 'setPreApprovedResult', null),
    withHandlers(handlers)
)(Result)
