import React from 'react';
import {
	Container,
	ColLeft,
	ColRight,
	Title,
	CardLimitApprove,
	CardLimitApproveTitle,
	CardLimitApproveAmount,
	CardLimitApproveExpire,
	CardLimitApprovePrepare,
	PhraseUnderLine,
	ProjectInfo,
	ProjectInfoRow,
	ProjectInfoLeft,
	ProjectInfoRight,
	IconCheckbox,
	ButtonContainer,
	ButtonConfirm,
	ButtonEdit,
	MonthlyPaymentLeft,
	MonthlyPaymentRight
} from './Result.style';
import iconResult from './assets/icon_result.svg';
import { KEY } from '../Overview/Overview.handler';
import axios from 'axios';
import { RequestMock } from './ResultDataMock';
import { formatCurrency } from '../../../utils/currency.util'

const DEV_URL = 'http://10.101.4.252:3000/dma-lead/develop/partner/v1/pre-approved/widget';


const Result: React.FunctionComponent = (props: any) => {

	const userProfile = JSON.parse(localStorage.getItem(KEY.GG_PROFILE) || '{}');

    const getPreApprovedResult = () => {
		return axios
			.post(
				DEV_URL,
                {...RequestMock},
				{
					headers: {
						'Content-Type': 'application/json',
						'Id-Token': userProfile.tokenId
					}
				}
			)
			.then((response) => {
				console.log('ON_GET_PRE_APPROVED_RESULT_SUCCESS', response)
				props.ON_GET_PRE_APPROVED_RESULT_SUCCESS(response.data);
			})
			.catch((error) => {
				console.log(error);
			});
	}

	const { preApprovedResult} = props

	return (
		<Container>
			<ColLeft>
				<Title>Chúc mừng {userProfile.name} </Title>

				<CardLimitApprove>
					<CardLimitApproveTitle>
						Hạn mức phê duyệt trước cho khoản vay mua nhà Quý khách là:
					</CardLimitApproveTitle>
					<CardLimitApproveAmount>
						{formatCurrency({
							value: preApprovedResult?.loanAmount || 0,
							showCurrency: true
						})}
					</CardLimitApproveAmount>
					<CardLimitApproveExpire>Hạn mức có hiệu lực đến ngày:
						{preApprovedResult?.expireDate || ' '}
					</CardLimitApproveExpire>
					<CardLimitApprovePrepare>
						<PhraseUnderLine>Hồ sơ cần chuẩn bị</PhraseUnderLine>
					</CardLimitApprovePrepare>
				</CardLimitApprove>

				<ProjectInfo>
					<ProjectInfoRow>
						<ProjectInfoLeft>Dự án: </ProjectInfoLeft>
						<ProjectInfoRight>{preApprovedResult?.projectName}</ProjectInfoRight>
					</ProjectInfoRow>
					<ProjectInfoRow>
						<ProjectInfoLeft>Thời gian vay dự kiến: </ProjectInfoLeft>
						<ProjectInfoRight>{preApprovedResult?.tenorInMonth}</ProjectInfoRight>
					</ProjectInfoRow>
					<ProjectInfoRow>
						<ProjectInfoLeft>Phương thức trả nợ: </ProjectInfoLeft>
						<ProjectInfoRight>{preApprovedResult?.paymentMethod}</ProjectInfoRight>
					</ProjectInfoRow>
					<ProjectInfoRow>
						<ProjectInfoLeft>Số tiền trả nợ tối đa: </ProjectInfoLeft>
						<ProjectInfoRight>
						{preApprovedResult?.monthlyPayment !== 0 &&
							formatCurrency({
							value: preApprovedResult?.monthlyPayment || 0,
							showCurrency: true
						})}
						</ProjectInfoRight>
					</ProjectInfoRow>
				</ProjectInfo>

				<ProjectInfoRow>
					<MonthlyPaymentLeft>Số tiền trả nợ hàng tháng xem tại: </MonthlyPaymentLeft>
					<MonthlyPaymentRight>
						<PhraseUnderLine>
							<div
							// onClick={showPopup(POPUP_NAME.REPAYMENT_PLAN)}
							>
								{'0'}
							</div>
						</PhraseUnderLine>
					</MonthlyPaymentRight>
				</ProjectInfoRow>
				<ProjectInfoRow>
					<IconCheckbox
						onClick={props.setAcceptLoanAmount}
						// isAccept={isAccept}
					/>Tôi đồng ý với hạn mức và &nbsp;<PhraseUnderLine
					//    onClick={showPopup(POPUP_NAME.LOAN_CONDITION)}
					>
						Điều kiện vay vốn
					</PhraseUnderLine>
				</ProjectInfoRow>
				<ButtonContainer>
					<ButtonConfirm id="pre_accept_loan" enableAgreeButton={true} onClick={props.ON_ACCEPT}>
						Đồng ý hạn mức phê duyệt
					</ButtonConfirm>
					<ButtonEdit onClick={getPreApprovedResult} id="pre_change_limit_loan" disabled={false}>
						Muốn thay đổi hạn mức
					</ButtonEdit>
				</ButtonContainer>
			</ColLeft>
			<ColRight>
				<img src={iconResult} alt='iconResult' />
			</ColRight>
		</Container>
	);
};

export default Result;
