const RequestMock = {
	"demographicInfo": {
		"accommodationType": "2",
		"dateOfBirth": "1980-10-14",
		"educationDegree": "UNI",
		"email": "string",
		"maritalStatus": "MAR",
		"name": "Anonymous",
		"transportationType": "AUT",
		"branch": "SGO",
		"province": "ID_58",
		"teamCode": "ANPD"
	},
	"loanPreference": {
		"loanAmount": 2000000000,
		"loanProduct": "AL",
		"loanTermInYear": 3,
		"carProduct": {
			"carCategory": "SUV",
			"carProvider": "VIN"
		}
	},
	"companyIncome": [
		{
			"capitalContributionRatio": 100,
			"index": 0,
			"ownerShip": "1",
			"recentAverageRevenue": 12000000,
			"recentAverageProfit": 10000000
		}
	],
	"historicalCredit": {
		"hasCreditCard": true,
		"hasOtherLoan": true,
		"otherLoans": [
			{
				"creditLimit": 50000000,
				"index": 0,
				"loanType": "CREDIT_CARD",
				"monthlyInstallment": 0,
				"ownerShipCode": "SELF",
				"payOff": false,
				"tenorInMonth": 12
			},
			{
				"creditLimit": 20000000,
				"index": 0,
				"loanType": "CREDIT_CARD",
				"monthlyInstallment": 0,
				"ownerShipCode": "SPOUSE",
				"payOff": false,
				"tenorInMonth": 12
			}
		]
	},
	"houseHoldIncome": [
		{
			"averageProfitInLast3months": 100000001,
			"averageRevenueInLast3months": 50000001,
			"businessLine1": "",
			"businessLine2": "",
			"businessLine3": "HKD_1_001",
			"businessType": "",
			"city": "ID_3",
			"index": 0,
			"ownerShip": "SELF"
		}
	],
	"houseRentIncome": [
		{
			"incomeAmount": 20000000,
			"index": 0,
			"location": "ID_03",
			"paymentMethod": "test",
			"typeOfProperty": "test"
		}
	],
	"identityDocuments": [
		{
			"id": "187171299",
			"index": 0,
			"type": "NATIONAL_ID"
		}
	],
	"pension": [
		{
			"income": 1000000,
			"index": 0,
			"ownerShip": "SELF",
			"paymentMode": "2"
		}
	],
	"roaApartment": [
		{
			"acceptedDeclareAmount": 1000000,
			"accountedForAmount": 10000000,
			"hasRevenuesFromLiq": "N",
			"index": 0
		}
	],
	"roaEState": [
		{
			"acceptedDeclareAmount": 1000000,
			"accountedForAmount": 10000000,
			"hasRevenuesFromLiq": "Y",
			"index": 0
		}
	],
	"roaSaving": [
		{
			"acceptedDeclareAmount": 1000000,
			"accountedForAmount": 10000000,
			"hasRevenuesFromLiq": "Y",
			"index": 0
		}
	],
	"roaTCBond": [
		{
			"acceptedDeclareAmount": 1000000,
			"accountedForAmount": 10000000,
			"hasRevenuesFromLiq": "N",
			"index": 0
		}
	],
	"salaryIncome": [
		{
			"blevel1": "PNC",
			"blevel2": "PNC_10",
			"blevel3": "PNC_10_1",
			"blevel4": "",
			"bprofession": "SAL",
			"companyCode": "",
			"index": 0,
			"isTop500": "N",
			"niemYet": "Y",
			"ownerShip": "SELF",
			"paymentType": "TCB",
			"position": "BCEO",
			"provableStableIncome": 12000000,
			"reputationLevel": "string",
			"tenureMonth": 6,
			"tenureYear": 3
		}
	],
	"vehicleIncome": [
		{
			"index": 0,
			"line": "12",
			"manufacturer": "BMW",
			"monthlyIncome": 10000000,
			"paymentMode": "test"
		}
	]
}

const ResponseMock = {
	data: {
		"status": "SUCCESS",
		"approvalResult": {
			"name": "Anonymous",
			"loanAmount": 2.6164177562341604E9,
			"tenorInMonth": 36,
			"paymentMethod": 2,
			"monthlyPayment": 9.578996118657288E7,
			"interestRate": 0.106,
			"originalGracePeriodInMonth": 0,
			"interestRateSupportMonth": 6
		},
		"errorCode": null,
		"errorName": null
	}
}

export {RequestMock, ResponseMock}