import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'
import checked_icon from './assets/checked_icon.svg'
import uncheck_icon from './assets/uncheck_icon.svg'


const Container = withClassName('flex flex-row w-full mb-16 ', 'Container')(
    styled('div')()
)
const ColLeft = withClassName('md:w-2/3 sm:w-full')(
    styled('div')(({ theme }) => ({
        paddingLeft: rem(16),
        paddingRight: rem(16),
        paddingTop: rem(30),
        [theme.mq.md]: {
            paddingLeft: rem(32),
            paddingTop: rem(24)
        }
    }))
)
const ColRight = withClassName('md:w-1/3 sm:w-full')(
    styled('div')(({ theme }) => ({
        display: 'none',
        [theme.mq.md]: {
            display: 'flex',
            justifyContent: 'center'
        }
    }))
)
const Title = withClassName()(
    styled('div')({
        fontSize: rem(20),
        fontWeight: 500,
        color: '#1D1D1F'
    })
)
const CardLimitApprove = withClassName('w-full flex py-4')(
    styled('div')({
        backgroundColor: '#FEEFEF',
        marginTop: rem(18),
        alignItems: 'center',
        flexDirection: 'column'
    })
)
const CardLimitApproveTitle = withClassName()(
    styled('div')({
        color: '#1D1D1F',
        fontSize: rem(13),
        fontWeight: 500,
        marginTop: rem(5),
        marginBottom: rem(12),
        textAlign: 'center'
    })
)
const CardLimitApproveAmount = withClassName()(
    styled('div')({
        fontSize: rem(24),
        fontWeight: 500,
        color: '#EC1C24',
        textAlign: 'right'
    })
)
const CardLimitApproveExpire = withClassName()(
    styled('div')({
        fontSize: rem(13),
        color: '#1D1D1F',
        marginTop: rem(10),
        marginBottom: rem(10)
    })
)
const CardLimitApprovePrepare = withClassName()(styled('div')({}))
const PhraseUnderLine = withClassName()(
    styled('a')({
        fontSize: rem(14),
        color: '#EC1C24',
        textDecoration: 'underline'
    })
)
const ProjectInfo = withClassName('')(
    styled('div')({
        marginTop: rem(20),
        marginBottom: rem(20)
    })
)
const ProjectInfoRow = withClassName('flex')(
    styled('div')({
        marginTop: rem(10),
        marginBottom: rem(10),
        alignItems: 'center'
    })
)
const ProjectInfoLeft = withClassName('')(
    styled('div')(({ theme }) => ({
        width: '50%',
        [theme.mq.md]: {
            width: '60%'
        }
    }))
)

const ProjectInfoRight = withClassName('flex')(
    styled('div')(({ theme }) => ({
        width: '50%',
        [theme.mq.md]: {
            justifyContent: 'flex-end',
            width: '40%'
        }
    }))
)

const IconCheckbox = withClassName('flex-no-shrink cursor-pointer')(
    styled('span')(
        {
            width: rem(20),
            height: rem(20),
            marginRight: rem(10)
        },
        ({ isAccept }: { isAccept: boolean }) => ({
            content: `url(${isAccept
                ? checked_icon
                : uncheck_icon
                })`
        })
    )
)
const ButtonContainer = withClassName('flex flex-wrap')(
    styled('div')({
        justifyContent: 'space-between',
        marginTop: rem(10)
    })
)
const ButtonConfirm = withClassName('w-full uppercase text-base text-white')(
    styled('button')(
        {
            height: rem(40),
            borderRadius: rem(5),
            lineHeight: rem(24),
            marginTop: rem(20)
        },
        ({
            enableAgreeButton,
            theme
        }: {
            enableAgreeButton: boolean
            theme: any
        }) => ({
            backgroundColor: enableAgreeButton ? '#EC1C24' : '#C5C8CE',
            cursor: enableAgreeButton ? 'pointer' : 'not-allowed',
            pointerEvents: enableAgreeButton ? 'initial' : 'none',
            [theme.mq.md]: {
                width: '48%'
            }
        })
    )
)

const ButtonEdit = withClassName('w-full uppercase text-base')(
    styled('button')(
        ({ theme, disabled }: { theme: any; disabled: boolean }) => ({
            height: rem(40),
            borderRadius: rem(5),
            border: disabled ? `${rem(1)} solid #C5C8CE` : `${rem(1)} solid #EC1C24`,
            color: disabled ? '#C5C8CE' : '#EC1C24',
            cursor: disabled ? 'default' : 'pointer',
            marginTop: rem(20),
            lineHeight: rem(24),
            [theme.mq.md]: {
                width: '48%'
            }
        })
    )
)

const MonthlyPaymentLeft = withClassName()(
    styled('div')(({ theme }) => ({
        // width: '65%',
        [theme.mq.md]: {
            width: 'auto'
        }
    }))
)
const MonthlyPaymentRight = withClassName('flex')(
    styled('div')(({ theme }) => ({
        width: '35%',
        justifyContent: 'flex-end',
        paddingLeft: rem(0),
        [theme.mq.md]: {
            width: 'auto',
            paddingLeft: rem(10),
            justifyContent: 'flex-start'
        }
    }))
)

export {
    Container,
    ColLeft,
    ColRight,
    Title,
    CardLimitApprove,
    CardLimitApproveTitle,
    CardLimitApproveAmount,
    CardLimitApproveExpire,
    CardLimitApprovePrepare,
    PhraseUnderLine,
    ProjectInfo,
    ProjectInfoRow,
    ProjectInfoLeft,
    ProjectInfoRight,
    IconCheckbox,
    ButtonContainer,
    ButtonConfirm,
    ButtonEdit,
    MonthlyPaymentLeft,
    MonthlyPaymentRight
}
