import PreApproved from './PreApproved.view'
import { compose, withState, withHandlers } from 'recompose'
import handlers from './PreApproved.hander'

export default compose(
    withState('currentStep', 'setStep', ''),
    withHandlers(handlers)
)(PreApproved)
