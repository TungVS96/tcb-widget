import React from 'react'
import { logoTechcombank } from '../Common.view'
import { Container } from './PreApproved.style'
import { match } from 'match-values'
import { STEP } from './PreApproved.type'
import Overview from './Overview/Overview.page'
import Result from './Result/Result.page'

const PreApproved: React.FunctionComponent = (props: any) => {
    const {
        currentStep
    } = props
    const Form = match(currentStep, {
        [STEP.step1]: Result,
        _: Overview
    })

    return (
        <Container>
            {logoTechcombank()}
            <Form
                goNext={props.GO_NEXT} 
            />
        </ Container>
    )
}

export default PreApproved
