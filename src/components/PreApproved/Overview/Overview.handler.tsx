import { STEP } from '../PreApproved.type';

export enum KEY {
	GG_PROFILE = 'gg-profile'
}

interface IGoogleProfile {
	tokenId: string;
	email: string;
	name: string;
}

export default {
	ON_GG_LOGIN_SUCCESS: (props: any) => (response: any) => {
		console.log('ON_GG_LOGIN_SUCCESS', response.tokenId)
		const userProfile: IGoogleProfile = {
			tokenId: response.tokenId,
			email: response.profileObj.email,
			name: response.profileObj.name
		};
		localStorage.setItem(KEY.GG_PROFILE, JSON.stringify(userProfile));
		props.goNext(STEP.step1);
	},
	ON_GG_LOGIN_FAILURE: () => () => {
		alert('ON_GG_LOGIN_FAILURE');
		localStorage.setItem(KEY.GG_PROFILE, '{}');
	}
};
