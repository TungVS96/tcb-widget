import React from 'react';
import icStep1 from './assets/step1.svg';
import icStep2 from './assets/step2.svg';
import icStep3 from './assets/step3.svg';
import { Container, Title, ThreStepWrapper, Step, StepTitle, Image, Button } from './Overview.style';
import { t } from '../../../i18n';
import { STEP } from '../PreApproved.type';
import { useGoogleLogin, GoogleLogout } from 'react-google-login';
import { KEY } from './Overview.handler';

const CLIENT_ID = '30985363837-kagbnc6vpu3t23468bec4ggo9rq1cksi.apps.googleusercontent.com';

const stepItems = [
	{
		src: icStep1,
		title: 'PreApprovedApplication.header.1'
	},
	{
		src: icStep2,
		title: 'PreApprovedApplication.header.2'
	},
	{
		src: icStep3,
		title: 'PreApprovedApplication.header.3'
	}
];

const userProfile = JSON.parse(localStorage.getItem(KEY.GG_PROFILE) || '{}');

const Overview: React.FunctionComponent = (props: any) => {

	const goPreApprovedForm = () => {
		if (userProfile && userProfile.tokenId ) {
            return props.goNext(STEP.step1);
        }
        signIn();
	};

	const { signIn } = useGoogleLogin({
		onSuccess: props.ON_GG_LOGIN_SUCCESS,
		onFailure: props.ON_GG_LOGIN_FAILURE,
		// isSignedIn: true,
		clientId: CLIENT_ID,
		accessType: 'offline'
    });

    const onLogoutSuccess = () => {
        console.log('onLogoutSuccess')
		localStorage.removeItem(KEY.GG_PROFILE);
    }

	return (
		<Container>
			<Title>{t('preApprovedOverView.label1')}</Title>
			<ThreStepWrapper>
				{stepItems.map((item, index: number) => {
					return (
						<Step key={index.toString()}>
							<Image src={item.src} alt={'step-icon'} />
							<StepTitle>{t(item.title)}</StepTitle>
						</Step>
					);
				})}
			</ThreStepWrapper>
			<Title>{t('preApprovedOverView.label2')}</Title>

            <Button onClick={goPreApprovedForm}>{t('preApprovedOverView.start')}</Button>

            {userProfile && userProfile.tokenId &&
                <GoogleLogout
                    clientId = {CLIENT_ID}
                    onLogoutSuccess = {onLogoutSuccess}
                />
            }
		</Container>
	);
};

export default Overview;
