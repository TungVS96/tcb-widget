import { rem, withClassName } from 'dma-ui'
import styled from 'react-emotion'

const Container = withClassName('flex flex-col text-sm items-center')(
    styled('div')(({ theme }: { theme: any }) => ({
        color: '#1D1D1F',
        padding: `${rem(38)} ${rem(16)}`,
        [theme.mq.md]: {
            padding: `${rem(48)} ${rem(238)} 0 ${rem(238)}`
        }
    }))
)

const ThreStepWrapper = withClassName('flex flex-col justify-between border')(
    styled('div')(({ theme }: { theme: any }) => ({
        margin: `${rem(20)} 0`,
        padding: rem(16),
        borderRadius: rem(2),
        [theme.mq.md]: {
            flexDirection: 'row',
            padding: `${rem(44)} ${rem(25)} ${rem(30)} ${rem(25)}`
        }
    }))
)

const Step = withClassName('flex flex-row items-center')(
    styled('div')(({ theme }: { theme: any }) => ({
        marginBottom: rem(25),
        ':last-child': {
            marginBottom: 0
        },
        [theme.mq.md]: {
            flexDirection: 'column'
        }
    }))
)

const StepTitle = withClassName('text-center')(
    styled('p')(({ theme }: { theme: any }) => ({
        lineHeight: rem(20),
        marginLeft: rem(8),
        [theme.mq.md]: {
            marginTop: rem(30)
        }
    }))
)

const Title = withClassName('md:text-center')(
    styled('p')({
        lineHeight: rem(20)
    })
)

const Button = withClassName(
    'bg-brand shadow text-white text-sm font-bold text-center uppercase'
)(
    styled('button')(({ theme }: { theme: any }) => ({
        height: rem(40),
        borderRadius: rem(5),
        marginTop: rem(24),
        width: rem(343),
        [theme.mq.md]: {
            margin: `${rem(24)} 0`
        }
    }))
)

const Image = withClassName('')(
    styled('img')(({ theme }: { theme: any }) => ({
        height: rem(64),
        width: rem(100),
        [theme.mq.md]: {
            height: rem(128),
            width: rem(206),
            marginLeft: rem(30),
            marginRight: rem(30)
        }
    }))
)

export { Container, ThreStepWrapper, Step, StepTitle, Title, Button, Image }
