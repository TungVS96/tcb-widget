import Overview from './Overview.view';
import { compose, withHandlers } from 'recompose';
import handlers from './Overview.handler';

export default compose(
	withHandlers(handlers)
)(Overview);
