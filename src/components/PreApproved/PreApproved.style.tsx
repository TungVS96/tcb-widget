import styled from 'react-emotion'

const Container = styled('div')({
    fontFamily: 'Roboto, Segoe UI Regular Vietnamese, Segoe UI, Segoe WP, Tahoma, Arial, sans-serif'
})

export { Container }