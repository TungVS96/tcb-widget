import { Intent, Spinner } from '@blueprintjs/core'
import { rem } from 'dma-ui'
import React from 'react'
import { keyframes } from 'react-emotion'

const spin = keyframes`
  from { transform: rotate(0deg); }
  to { transform: rotate(360deg); }

`

const Loading: React.FunctionComponent<any> = ({ children, style, isSpinnerWhite }) => (
  <div className="flex items-center justify-center h-screen z-20" style={style}>
    {isSpinnerWhite ? (
      <div
        style={{
          border: `${rem(4)} solid #ccc`,
          borderTop: `solid ${rem(4)} #fff`,
          borderRadius: rem(20),
          width: rem(40),
          height: rem(40),
          animation: `${spin} 1s linear infinite`,
          alignContent: 'flex-start'
        }}
      />
    ) : (
        <Spinner intent={Intent.PRIMARY} size={50} />
      )}
    <p style={{ marginTop: rem(10) }}>{children}</p>
  </div>
)

export default Loading
