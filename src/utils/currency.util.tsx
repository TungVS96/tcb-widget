import I18n from '../i18n'

const trimCurrency = (value: string): string =>
  (value || '').toString().replace(/[^\d.]+/g, '')

const formatCurrency = ({
  value,
  precision = 0,
  showCurrency = false
}: {
  value: string | number
  precision?: number
  showCurrency?: boolean
}): string => {
  const numberValue = typeof value === 'string' ? toNumber(value) : value

  const formattedValue = I18n.toNumber(numberValue, {
    precision,
    strip_insignificant_zeros: true
  })
  if (showCurrency) {
    return `${formattedValue} VNĐ`
  }

  return `${formattedValue}`
}

const toNumber = (value: string) => {
  const trimmedValue = trimCurrency(value || '')
  const numberValue = parseFloat(trimmedValue)

  return isNaN(numberValue) ? 0 : numberValue
}

export { toNumber, formatCurrency, trimCurrency }
