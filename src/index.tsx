import React from 'react';
import ReactDOM from 'react-dom';
import PreApproved from './components/PreApproved/PreApproved.page';
import { theme } from 'dma-ui'
import { ThemeProvider } from 'emotion-theming'
import './theme/index.css'

const render = () => {

  return (
    <ThemeProvider theme={theme}>
      <PreApproved />
    </ThemeProvider>
  )
}
ReactDOM.render(
  render(),
  document.getElementById('root')
);